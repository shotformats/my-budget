package com.shotformats.mybudget.util;

/**
 * @author Ajitsharma
 */
public interface Constants {
    public long SPLASH_MINIMUM_TIME = 3000L;// 3 sec.

    public interface ACTIVITY_REQUEST_RESPONSE_CODE {
        //guiding_tour
        public int GUIDING_TOUR = 201;

        public int REGISTRATION_SCREEN = 202;

        public int LOGIN_SCREEN   = 203;
        //Profile details Screen
        public int PROFILE_SCREEN = 204;
    }

    public interface URLS {
        public int CONNECT_TIME_OUT = 10 * 1000;
        public int READ_TIME_OUT    = 10 * 1000;
        String host = "";
        String HELP = "";

    }

    public interface REQUEST {
        int CAMERA      = 101;
        int SELECT_FILE = 102;
    }

    interface CHARSET {

        String UTF        = "UTF-8";
        String ASCII      = "US-ASCII";
        String ISO_8859_1 = "ISO-8859-1";
    }

    enum LANDING_PAGE {
        HOME,
        REGISTRATION,
        GUIDING_TOUR,
    }

    public interface USER_TYPE {
        int GUEST = 1;
        int APP   = 2;
    }

    public interface NAVIGATION_ITEM {
        public int HEADER = 0;
        public int ITEM   = 1;
        public int FOOTER = 2;

    }

}
