package com.shotformats.mybudget.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.chyrta.onboarder.OnboarderActivity;
import com.chyrta.onboarder.OnboarderPage;
import com.shotformats.mybudget.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajitsharma on 6/23/2016.
 */
public class IntroActivity extends OnboarderActivity {

    List<OnboarderPage> onboarderPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onboarderPages = new ArrayList<OnboarderPage>();

        // Create your first page
        OnboarderPage onboarderPage1 = new OnboarderPage("Title 1", "Description 1");
        OnboarderPage onboarderPage2 = new OnboarderPage("Title 2", "Description 2", R.drawable.ic_menu_camera);

        // You can define title and description colors (by default white)
        onboarderPage1.setTitleColor(R.color.colorBlack);
        onboarderPage1.setDescriptionColor(R.color.colorBlack);

        // Don't forget to set background color for your page
        onboarderPage1.setBackgroundColor(R.color.colorGray);

        onboarderPage2.setTitleColor(R.color.colorBlack);
        onboarderPage2.setDescriptionColor(R.color.colorBlack);

        // Don't forget to set background color for your page
        onboarderPage2.setBackgroundColor(R.color.colorGray);

        // Add your pages to the list
        onboarderPages.add(onboarderPage1);
        onboarderPages.add(onboarderPage2);

        // And pass your pages to 'setOnboardPagesReady' method
        setOnboardPagesReady(onboarderPages);

    }

    @Override
    public void onSkipButtonPressed() {
        // Optional: by default it skips onboarder to the end
        super.onSkipButtonPressed();
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        finish();
        // Define your actions when the user press 'Skip' button
    }

    @Override
    public void onFinishButtonPressed() {
        // Define your actions when the user press 'Finish' button
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        finish();
    }


}
