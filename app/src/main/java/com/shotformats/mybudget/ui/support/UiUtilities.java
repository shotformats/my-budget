package com.shotformats.mybudget.ui.support;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.widget.Toast;

import com.shotformats.mybudget.util.Constants;

public class UiUtilities {

    private static String TAG  = "UiUtilities";
    private static Dialog dialog;


    /**
     * This method will restart application from launcher activity.
     *
     * @param activity
     */
    public static void restartApplication(Activity activity) {
        Intent i = activity.getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(activity.getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(i);
    }



    /**
     * Should be used when same toast should not come in non-debug build
     *
     * @param mContext
     * @param text
     */
    public static void showToast(Context mContext, String text) {
            Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    /**
     * Should be used when same toast should not come in non-debug build
     *
     * @param mContext
     * @param id
     */
    public static void showToast(Context mContext, int id) {
            Toast.makeText(mContext, mContext.getString(id), Toast.LENGTH_LONG)
                    .show();
    }

    /**
     * Before calling this method insure calling OnActivityResult in target activity
     *
     * @param target
     */
    public static void doSelectImage(final Activity target, String title) {
        final CharSequence[] items   = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder  builder = new AlertDialog.Builder(target);
        builder.setTitle(title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    target.startActivityForResult(intent, Constants.REQUEST.CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    target.startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            Constants.REQUEST.SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

 }
