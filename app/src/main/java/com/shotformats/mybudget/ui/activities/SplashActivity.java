package com.shotformats.mybudget.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.shotformats.mybudget.R;
import com.shotformats.mybudget.util.Constants;

public class SplashActivity extends AppCompatActivity implements Constants.ACTIVITY_REQUEST_RESPONSE_CODE {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GUIDING_TOUR) {
            showNextScreen(Constants.LANDING_PAGE.GUIDING_TOUR);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static String TAG = SplashActivity.class.getName();

    Handler  mHideHandler = new Handler();
    Runnable mShowHome    = new Runnable() {
        @Override
        public void run() {
//            showHomeScreen(SplashActivity.this, true);
            Intent homeIntent = new Intent(SplashActivity.this, HomeActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
    };

    Runnable mShowGuidingTour = new Runnable() {
        @Override
        public void run() {
            Intent homeIntent = new Intent(SplashActivity.this, IntroActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
//            showGuidingTourScreen(SplashActivity.this, false, GUIDING_TOUR);
//            Intent regIntent = new Intent(SplashActivity.this, GuidingTourActivity.class);
//            startActivityForResult(regIntent, GUIDING_TOUR);
        }
    };

    Runnable mShowReg = new Runnable() {
        @Override
        public void run() {
//            showRegistrationScreen(instance, RegistrationActivity.REGISTRATION_PROCESS_STARTED, true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //here we will check for registration
        showNextScreen(Constants.LANDING_PAGE.GUIDING_TOUR);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     * <p/>
     *
     * @param page
     *         will be used to detect next screen
     */
    private void showNextScreen(Constants.LANDING_PAGE page) {
        switch (page) {
            case HOME:
                mHideHandler.postDelayed(mShowHome, Constants.SPLASH_MINIMUM_TIME);
                break;
            case REGISTRATION:
                break;
            case GUIDING_TOUR:
                mHideHandler.postDelayed(mShowGuidingTour, Constants.SPLASH_MINIMUM_TIME);
                break;
            default:
                mHideHandler.post(mShowHome);
                break;
        }
    }
}