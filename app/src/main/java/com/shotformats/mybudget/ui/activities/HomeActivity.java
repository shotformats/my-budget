package com.shotformats.mybudget.ui.activities;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.shotformats.mybudget.R;
import com.shotformats.mybudget.model.DrawerItem;
import com.shotformats.mybudget.ui.support.adapters.CustomDrawerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ajitsharma
 *         <p/>
 *         Do not use butterknife with this class because of throwing nullpointer exception while
 *         making object of Drawer.
 *         <p/>
 *         # future steps :- We will find any way to use knife here
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private DrawerLayout mDrawerLayout;
    private ListView     mDrawerList;
    CustomDrawerAdapter adapter;
//    private BottomSheetBehavior mBottomSheetBehavior;
    private static final String TAG = "Home Screen";

    List<DrawerItem> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dataList = new ArrayList<DrawerItem>();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        //code to add header and footer to listview
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.nav_header, mDrawerList,
                false);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.nav_footer, mDrawerList,
                false);
        mDrawerList.addHeaderView(header, null, false);
        mDrawerList.addFooterView(footer, null, false);
        try {
            findViewById(R.id.profile_image).setOnClickListener(this);
            findViewById(R.id.profile_name).setOnClickListener(this);
            findViewById(R.id.nav_item_help).setOnClickListener(this);
            findViewById(R.id.nav_item_setting).setOnClickListener(this);
        } catch (NullPointerException e) {
            Log.e(TAG, e.toString());
        }

        dataList.add(new DrawerItem(R.string.nav_dashboard, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_add_expenses, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_set_budget, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_income, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_expenditure, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_points_earned, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_reminder, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_financial_advisors, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_asset_libilities, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_training, R.drawable.ic_menu_camera));
        dataList.add(new DrawerItem(R.string.nav_setting, R.drawable.ic_menu_camera));

        adapter = new CustomDrawerAdapter(this, R.layout.nav_item,
                dataList);

        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        selectItem(0);

//        View bottomSheet = findViewById(R.id.bottom_sheet);
//
//        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeActivity/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            selectItem(position);
        }
    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void selectItem(int possition) {
        switch (possition) {
            case 0:
                setTitle(getString(((DrawerItem)dataList.get(possition)).getItemNameId()));
                break;
            case 1:
                setTitle(getString(((DrawerItem)dataList.get(possition-1)).getItemNameId()));
                break;
            case 2:
                setTitle(getString(((DrawerItem)dataList.get(possition-1)).getItemNameId()));
                break;
            case 3:
                setTitle(getString(((DrawerItem)dataList.get(possition-1)).getItemNameId()));
                break;
            case 4:
                setTitle(getString(((DrawerItem)dataList.get(possition-1)).getItemNameId()));
                break;
            case 5:
                setTitle(getString(((DrawerItem)dataList.get(possition-1)).getItemNameId()));
                break;
            default:
                setTitle(getString(((DrawerItem)dataList.get(possition-1)).getItemNameId()));
                break;
        }
        mDrawerList.setItemChecked(possition, true);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_image:
                Toast.makeText(HomeActivity.this, "Profile image clicked", Toast.LENGTH_SHORT).show();
                closeDrawer();
//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.profile_name:
                Toast.makeText(HomeActivity.this, "Profile name clicked", Toast.LENGTH_SHORT).show();
                closeDrawer();
                break;
            case R.id.nav_item_help:
                Toast.makeText(HomeActivity.this, "help clicked", Toast.LENGTH_SHORT).show();
                closeDrawer();
                break;
            case R.id.nav_item_setting:
                Toast.makeText(HomeActivity.this, "setting clicked", Toast.LENGTH_SHORT).show();
                closeDrawer();
                break;
        }


    }
}
