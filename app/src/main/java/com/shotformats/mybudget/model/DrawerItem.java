package com.shotformats.mybudget.model;

/**
 * Created by Ajitsharma on 6/23/2016.
 */
public class DrawerItem {

    int _nameId;
    int _imageId;

    public DrawerItem(int name, int imageId) {
        super();
        _nameId = name;
        _imageId = imageId;
    }

    public int getItemNameId() {
        return _nameId;
    }

    public void setItemNameId(int itemName) {
        _nameId = itemName;
    }

    public int getImgResID() {
        return _imageId;
    }

    public void setImgResID(int imgResID) {
        this._imageId = imgResID;
    }

}
